# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.11.3] - 2025-03-01

### Fixed

- Add support for subdomain Medium format URLs to download articles. (@miry)

## [0.11.2] - 2025-03-01

### Fixed

- Add support for YouTube Shorts embeds. (@miry)
- Extract the YouTube ID from the thumbnailUrl iframe parameter. (@miry)

## [0.11.1] - 2024-12-01

### Changed

- Medium has revised the base path for images by introducing `/v2`.
  By default, it seamlessly redirects from the previous endpoints to the new one.
  To optimize efficiency, minimize the number of requests and ensure to include `/v2`
  in the image path when making requests. (@miry)
- Include the API version header when making Gist REST API requests.
  Additionally, implement a fallback mechanism that allows sending requests
  without an authorization token to the Gist API, in case token is not valid. (@miry)
- Update the DevTo post's cover image assets filename to exclude encoded URI components
  and sanitization. (@miry)

## [0.11.0] - 2024-01-04

### Changed

- Adjust the HTTP connection timeouts explicitly to prevent indefinite blocking of medup. (@miry)
- Ensure that logs consistently display the full domain in request URL log records. (@miry)

### Added

- Include Author and Username in Page Headers for DevTo Integration. (https://codeberg.org/miry/medup/pulls/33, @miry)

## [0.10.0] - 2023-11-26

### Changed

- In the medup release, the `--debug` option is now enabled by default,
  providing meaningful backtraces for errors. (@miry)
- Update API version for DevTo integration. Switched to [Forem API V1](https://developers.forem.com/api/v1). (https://codeberg.org/miry/medup/issues/24, @miry)
- Ensure uniqueness for assets by adding the article's slug prefix to the asset's name. (https://codeberg.org/miry/medup/issues/27, @miry)

### Fixed

- Handle gracefully HTTP response 429 from DevTo api.
  (https://codeberg.org/miry/medup/issues/23, @miry)

## [0.9.0] - 2023-11-11

### Changed

- Medup now addresses compatibility issues with Windows filesystems by replacing
  special characters (`*`, `>`, `<`, and `|`) in asset filenames with underscores (\_).
  This update aims to resolve any issues related to these characters when working with filenames on Windows.
  (https://codeberg.org/miry/medup/issues/19, @miry)

## [0.8.2] - 2023-11-02

### Added

- Improved Twitter iframe format compatibility by updating it to work with the latest Medium changes.
  This update also enhances the parser's capability to handle both the new and
  legacy Twitter formats, ensuring seamless support for older implementations.
  (https://codeberg.org/miry/medup/issues/12, @miry)
- Add short argument `-U` for `--update`. (@miry)

### Changed

- Move homebrew formula from github. (@miry)

### Fixed

- Some exported DevTo artciles have a double headers (one from Medup and second original DevTo).
  New changes ignores DevTo's headers in the Markdown document. (https://codeberg.org/miry/medup/issues/8, @miry)
- Handle 404 HTTP response for articles.
  Removed a hack related to Medium implementation that involved skipping
  the first 16 bytes to obtain valid JSON output for Devto adapter.
  This was necessary for Medium but not for DevTo, as DevTo returns valid JSON data
  without this hack. (https://codeberg.org/miry/medup/issues/9, @miry)
- Don't display the backtrace for missing DevTo articles.
  Instead, show an informative error message when using medium verbosity.
  This change enhances the user experience by providing more meaningful
  error messages while reducing unnecessary backtrace information.
  (https://codeberg.org/miry/medup/issues/9, @miry)
- When filtering articles based on author name, the system will now use the organization
  slug if it is required. DevTo utilizes the organization slug for articles.
  The response from posts API includes more suitable field such as 'path',
  which will be used in place of 'slug'.
  This change ensures more accurate and consistent article backup based on author names.
  (https://codeberg.org/miry/medup/issues/10, @miry)
- Allow to follow redirects for Medium image downloader.
  (https://codeberg.org/miry/medup/issues/7, @miry)

## [0.8.1] - 2022-08-18

### Changed

- Medium: use field `webCanonicalUrl` for `canonical_url`. (@miry)

## [0.8.0] - 2022-07-27

### Changed

- Tags is an array instead of the list of words split with coma. (@miry)

### Fixed

- Overlapping style and link tags could produce an incredible Markdown.
  Because of WYSIWYG, plenty of hidden things that could destroy Markdown.
  Introduce a workaround to have swap places last space and closing elements.
  Ignore links without anchor text.
  There are still more possibilities to break Markdown. (https://github.com/miry/medup/pull/59, @miry)

## [0.7.0] - 2022-07-22

### Changed

- Introduce a article attribute `canonical_url`. (@miry)
- Markdown header is YAML compatible. (@miry)

### Added

- Introduce `--dry-run` argument to test without filesystem modifications. (https://github.com/miry/medup/pull/53, @miry)

## [0.6.0] - 2022-07-09

### Added

- Introduce Dev.to integration. (https://github.com/miry/medup/pull/18, @miry)

### Changed

- Use crystal lang 1.5.0 (https://github.com/miry/medup/pull/56, @miry)

## [0.5.0] - 2022-05-09

### Added

- Allow to change the download distination for assets with flag `--assets-dir=<DIR>`. (https://github.com/miry/medup/pull/40, @miry)
- Allow to change the uri path for assets inside document with `--assets-base-path=<BASE_PATH>`.
  It help to access assets in case the global assets path or custom directory. (https://github.com/miry/medup/pull/40, @miry)
- Support Demo with Bridgetown via command `rake demo:bridgetown:serve`. (@miry)

## [0.4.1] - 2022-05-02

### Changed

- Update article' url tag to get value from response, instead of from commandline
  arguments (@miry)
- Update the code to use **Context** pattern. Combine in it options, logger and
  settings. (https://github.com/miry/medup/pull/47, @miry)

### Added

- Allow to specify `MEDUP_GITHUB_API_TOKEN` environment variable to increase
  number of requests to gist. (https://github.com/miry/medup/pull/47, @miry)

## [0.4.0] - 2022-04-30

### Changed

- Inline gist media as code block inside a result markdown file. (https://github.com/miry/medup/pull/39, @miry)
- Inline youtube media as link instead of iframe. (https://github.com/miry/medup/pull/42, @miry)
- Inline twitter media as blockquote instead of iframe. (https://github.com/miry/medup/pull/44, @miry)
- Inline general embedy media as link with thumbnail image instead of iframe. (https://github.com/miry/medup/pull/45, @miry)

### Added

- Move most of debug output to logger. Allow to specify the verbosity of output
  with parameter `-v[NUM]`. Previous messages debug messages are appeared
  in `stderr`. (@miry)
- For rich elements with anotations or markups render next to iframe. (@miry)

## [0.3.0] - 2022-04-09

### Changed

- Update command line argument parse view. During error make sure the exit code is 1 (@miry)
- Use `podman` instead of `docker` (https://github.com/miry/medup/pull/35, @miry)
- Use crystal lang 1.4.0 (@miry)

### Fixed

- Custom domain posts returns excpetions, fixed the problem (@miry)
- Detect downloaded assets' type and add missing extension (https://github.com/miry/medup/pull/37, @miry)

### Added

- Add integration tests to test command line output (@miry)
- Extract targets(user, publication, single post) base on pattern. Download articles for different targets in same process. (https://github.com/miry/medup/pull/36, @miry)

## [0.2.1] - 2022-03-27

### Changed

- Use crystal version from shard for Dockerfile (@miry)

### Fixed

- Skip download assets images without option `--assets-images` (@miry)
- Emoji breaks markdown rendering (https://github.com/miry/medup/pull/34, @miry, @clawfire)

## [0.2.0] - 2022-03-20

### Changed

- Use crystal lang 1.3.2 (https://github.com/miry/medup/pull/31, @miry)

### Added

- Export posts from a publication with option `--publication=NAME` (https://github.com/miry/medup/pull/31, @miry, @clawfire)
- Allow to save images to assets folder with option `--assets-images` (https://github.com/miry/medup/pull/33, @miry, @clawfire)

## [0.1.10] - 2021-04-17

### Added

- Allow to download pure orginal JSON by url (@miry)

### Changed

- Add ISO8601 format date in the result post filename (@miry)
- Use crystal lang 0.36.1 (@miry)
- Use crystal lang 1.0.0 (@miry)
- Update docker image to use alpine instead ubuntu and static linked libs (@miry)

## [0.1.9] - 2020-09-17

### Changed

- Filename sanitization (https://github.com/miry/medup/pull/19, @miry)
- Embed images to single document (https://github.com/miry/medup/pull/3, @miry)
- Download posts in 2 concurent processes (https://github.com/miry/medup/pull/17, @miry)
- Use crystal lang 0.35.1

## [0.1.8] - 2020-05-22

### Changed

- Use crystal lang 0.33.0 for Docker containers
- Use crystal lang 0.34.0

## [0.1.7] - 2020-03-15

### Added

- Process only one article: medup <url> (https://github.com/miry/medup/pull/24, @miry)

## [0.1.6] - 2020-03-12

### Added

- Add command line argument to allow update overwrite file content (https://github.com/miry/medup/pull/16, @miry)
- Fix small parsing issues (https://github.com/miry/medup/pull/25, https://github.com/miry/medup/pull/27, @miry)

## [0.1.5] - 2020-01-21

### Added

- Markdown: Store subtitle, tags and SEO description information (https://github.com/miry/medup/pull/7, @miry)
- Markdown: Store authors information (@miry)
- Export user's recommened articles (https://github.com/miry/medup/pull/2, @miry)
- Don't raise exceptions for paragraph type 2: with images in background, title and alignment (https://github.com/miry/medup/pull/2, @miry)
- Print error messages to STDERR (https://github.com/miry/medup/pull/2, @miry)
- Specify import format via command line (https://github.com/miry/medup/pull/23, @miry)

### Changed

- Create missing subfolders with more than 1 layer (https://github.com/miry/medup/pull/2, @miry)

## [0.1.4] - 2019-12-28

### Added

- Markdown: Support inline formatting (https://github.com/miry/medup/pull/6, @miry)

## [0.1.3] - 2019-12-21

### Added

- Create Changelog
- Create Contributing document
- Download iframe components to assets

### Changed

- Use markdown comments for not implemented elements like IFRAME
- Add section to Readme to show exported vs original document view
- Split paragraphs with 2 empty lines

## [0.1.2] - 2019-12-21

### Changed

- Docker base image is ubuntu bionic
- Upgrade supported crystal language to 0.32.1

### Added

- Allow to release via `rake` tasks

## [0.1.1] - 2019-12-21

### Added

- Minimal markdown export format support

## [0.1.0] - 2019-12-04

### Added

- Initialize project structure
- Relase docker image
- Dump Medium posts base on author name

[Unreleased]: https://codeberg.org/miry/medup/compare/v0.11.3...HEAD
[0.11.3]: https://codeberg.org/miry/medup/compare/v0.11.2...v0.11.3
[0.11.2]: https://codeberg.org/miry/medup/compare/v0.11.1...v0.11.2
[0.11.1]: https://codeberg.org/miry/medup/compare/v0.11.0...v0.11.1
[0.11.0]: https://codeberg.org/miry/medup/compare/v0.10.0...v0.11.0
[0.10.0]: https://codeberg.org/miry/medup/compare/v0.9.0...v0.10.0
[0.9.0]: https://codeberg.org/miry/medup/compare/v0.8.2...v0.9.0
[0.8.2]: https://codeberg.org/miry/medup/compare/v0.8.1...v0.8.2
[0.8.1]: https://codeberg.org/miry/medup/compare/v0.8.0...v0.8.1
[0.8.0]: https://codeberg.org/miry/medup/compare/v0.7.0...v0.8.0
[0.7.0]: https://codeberg.org/miry/medup/compare/v0.6.0...v0.7.0
[0.6.0]: https://codeberg.org/miry/medup/compare/v0.5.0...v0.6.0
[0.5.0]: https://codeberg.org/miry/medup/compare/v0.4.1...v0.5.0
[0.4.1]: https://codeberg.org/miry/medup/compare/v0.4.0...v0.4.1
[0.4.0]: https://codeberg.org/miry/medup/compare/v0.3.0...v0.4.0
[0.3.0]: https://codeberg.org/miry/medup/compare/v0.2.1...v0.3.0
[0.2.1]: https://codeberg.org/miry/medup/compare/v0.2.0...v0.2.0
[0.2.0]: https://codeberg.org/miry/medup/compare/v0.1.10...v0.2.0
[0.1.10]: https://codeberg.org/miry/medup/compare/v0.1.9...v0.1.10
[0.1.9]: https://codeberg.org/miry/medup/compare/v0.1.8...v0.1.9
[0.1.8]: https://codeberg.org/miry/medup/compare/v0.1.7...v0.1.8
[0.1.7]: https://codeberg.org/miry/medup/compare/v0.1.6...v0.1.7
[0.1.6]: https://codeberg.org/miry/medup/compare/v0.1.5...v0.1.6
[0.1.5]: https://codeberg.org/miry/medup/compare/v0.1.4...v0.1.5
[0.1.4]: https://codeberg.org/miry/medup/compare/v0.1.3...v0.1.4
[0.1.3]: https://codeberg.org/miry/medup/compare/v0.1.2...v0.1.3
[0.1.2]: https://codeberg.org/miry/medup/compare/v0.1.1...v0.1.2
[0.1.1]: https://codeberg.org/miry/medup/compare/v0.1.0...v0.1.1
[0.1.0]: https://codeberg.org/miry/medup/releases/tag/v0.1.0

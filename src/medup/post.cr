require "json"

module Medup
  class Post
    @[JSON::Field(ignore: true)]
    property ctx : ::Medup::Context = ::Medup::Context.new

    @[JSON::Field(ignore: true)]
    property raw : String = ""

    @[JSON::Field(ignore: true)]
    @_filename : String? = nil

    property id : Int32?
    property slug : String = ""
    property created_at : Time = Time.utc

    def self.from_json(raw : String)
      parser = JSON::PullParser.new(raw)
      instance = new(parser)
      instance.raw = raw
      instance
    end

    def to_pretty_json
      return "" if @raw == ""
      JSON.parse(@raw).to_pretty_json
    end

    def to_md
      content, assets = "", Hash(String, String).new
      return content, assets
    end

    def filename
      @_filename ||= [created_at.to_s("%F"), slug].reject!(&.blank?).join("-") || ""
    end
  end
end

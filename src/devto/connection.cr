require "uri"

require "./error"

module Devto
  module Connection
    HOST                                      = "dev.to"
    ASSETS_FILENAME_SPECIAL_CHARACTERS_REGEXP = /[\*\>\<\|]+/

    def get(endpoint, params : Hash(String, String)? = nil, headers : HTTP::Headers? = nil, body : String? = nil)
      request_json "GET", endpoint, params, headers, body
    end

    def get_pagination(endpoint, params : Hash(String, String)? = nil, headers : HTTP::Headers? = nil, body : String? = nil, &block)
      params ||= Hash(String, String).new
      params["page"] ||= "1"
      params["per_page"] ||= "1000"

      page = params["page"].to_i
      while page < 1000
        response = get(endpoint, params, headers, body)
        break if response == "[]"
        yield response
        page += 1
        params["page"] = page.to_s
      end
    end

    def http(host : String? = HOST, format : String = "json") : HTTP::Client
      host ||= HOST
      if @http.has_key?(host)
        return @http[host].not_nil!
      end

      _http = HTTP::Client.new host, port: 443, tls: true
      _http.connect_timeout = 10.seconds
      _http.read_timeout = 30.seconds

      _http.before_request do |request|
        request.headers["User-Agent"] = "medup/#{Medup::VERSION}"

        if format == "json"
          request.headers["Content-Type"] = "application/json"
          request.headers["Accept"] = "application/vnd.forem.api-v1+json"
          # request.headers["Api-Key"] = "<work in progress>"
        end
      end

      @http[host] = _http
      return _http
    end

    def http_exec(uri, method, headers, body, format = "json")
      response = http(uri.host, format).exec(method: method.upcase, path: uri.request_target, headers: headers, body: body)
      @logger.info "#{method} #{uri} => #{response.status_code} #{response.status_message}"
      @logger.info 12, response.headers.to_s
      response
    end

    def request(method, endpoint, params : Hash(String, String)? = nil, headers : HTTP::Headers? = nil, body : String? = nil, format : String = "json")
      uri = URI.parse endpoint
      uri.host ||= HOST
      uri.scheme ||= "https"

      if params
        o = uri.query_params
        params.each { |k, v| o.add(k, v) }
        uri.query_params = o
      end

      response = http_exec(uri, method.upcase, headers, body, format)

      # Follow redirects
      limit = 10
      while limit > 0 && response.status_code >= 300 && response.status_code < 400
        redirect_uri = URI.parse(response.headers["location"])
        redirect_uri.host ||= uri.host
        redirect_uri.scheme ||= uri.scheme
        uri = redirect_uri
        response = http_exec(uri, method.upcase, headers, body, format)
        limit -= 1
      end

      mime = response.mime_type
      if mime && %w[text application].includes?(mime.type)
        @logger.info 12, response.body
      end

      error = ::Devto::Error.from_response(response)
      raise error if error

      response
    end

    def request_json(method, endpoint, params : Hash(String, String)? = nil, headers : HTTP::Headers? = nil, body : String? = nil)
      response = request(method, endpoint, params, headers, body, format: "json")

      error = ::Devto::Error.from_json_response(response)
      raise error if error

      response.body
    end

    def download(endpoint : String)
      response = request("GET", endpoint, format: "*")
      response.body
    end

    def download_image(name : String, endpoint : String)
      response = request("GET", endpoint, format: "*")

      filename = name
      ext = File.extname(filename)
      if ext == ""
        mt = response.mime_type
        if !mt.nil? && !mt.sub_type.nil?
          filename += "." if filename[-1] != '.'
          filename += mt.sub_type.not_nil!
        end
      end
      filename = filename.gsub(ASSETS_FILENAME_SPECIAL_CHARACTERS_REGEXP, '_')

      return response.body, response.content_type, filename
    end
  end
end

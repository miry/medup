require "yaml"

require "../medup/post"

module Devto
  class Post < ::Medup::Post
    include JSON::Serializable
    include JSON::Serializable::Unmapped

    class User
      include JSON::Serializable

      property name : String = ""
      property username : String = ""
    end

    @[JSON::Field(ignore: true)]
    property client : ::Devto::Client = ::Devto::Client.new(::Medup::Context.new)

    property title : String = ""
    property description : String = ""
    property url : String = ""
    property canonical_url : String = ""
    property cover_image : String = ""
    property body_markdown : String = ""
    property tag_list : String = ""
    property tags : Array(String) = [] of String
    property user : User?

    def to_md
      result = md_header
      assets = Hash(String, String).new
      footer = "\n"

      result += md_cover_image(assets)

      result += "# " + @title + "\n\n"

      content = md_content(assets)
      result += content + "\n\n"

      if !@ctx.settings.assets_image?
        footer += assets.map do |asset_name, asset_content|
          "[#{asset_name}]: #{asset_content}"
        end.join("\n")
      end

      result += footer

      return result, assets
    end

    def md_header
      header = {
        "url"           => @url,
        "canonical_url" => @canonical_url,
        "title"         => @title,
        "slug"          => @slug,
        "description"   => @description,
        "tags"          => @tags,
      }

      u = @user
      if u
        header["author"] = u.name if !u.name.nil? && !u.name.blank?
        header["username"] = u.username if !u.username.nil? && !u.username.blank?
      end

      header.to_yaml + "---\n\n"
    end

    def md_cover_image(assets)
      return "" if @cover_image == ""

      assets_base_path = @ctx.settings.assets_base_path
      asset_name = [filename, "cover_image", File.basename(URI.decode(@cover_image))].reject!(&.blank?).join("-")
      asset_body, asset_type, asset_name = download_image(asset_name, @cover_image)
      result = "![Cover image]"

      if @ctx.settings.assets_image?
        asset_id = Zaru.sanitize!(asset_name, fallback: "img_ref_cover_image")
        assets[asset_id] = asset_body
        result += "(#{assets_base_path}/#{asset_id})"
      else
        asset_id = "img_ref_cover_image"
        assets[asset_id] = "data:#{asset_type};base64," + \
          Base64.strict_encode(asset_body)

        result += "[#{asset_id}]"
      end

      result + "\n\n"
    end

    def md_content(assets) : String
      assets_base_path = @ctx.settings.assets_base_path

      # Exclude DevTo's yaml header section
      @body_markdown.sub(/\A\-\-\-\n((?!\-\-\-).)*^\-\-\-\n/m, "") \
        .gsub(/(?<image_start>!\[[^\]]*\])\((?<src>[^\)]*)\)/) do |_, match|
        src = match["src"]
        uri = URI.parse src
        asset_name = File.basename(uri.path)
        asset_body, asset_type, asset_name = download_image(asset_name, src)

        result = match["image_start"]
        if @ctx.settings.assets_image?
          asset_name = [filename, asset_name].reject!(&.blank?).join("-")
          assets[asset_name] = asset_body
          result += "(#{assets_base_path}/#{asset_name})"
        else
          asset_id = "img_ref_" + asset_name.gsub(".", "_")
          assets[asset_id] = "data:#{asset_type};base64," + \
            Base64.strict_encode(asset_body)
          result += "[" + asset_id + "]"
        end

        result
      end
    end

    def download_image(name : String, src : String)
      @client.download_image(name, src)
    end
  end
end

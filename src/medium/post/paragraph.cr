require "json_mapping"
require "zaru_crystal/zaru"

require "logger"

module Medium
  class Post
    class Paragraph
      ASSETS_FILENAME_SPECIAL_CHARACTERS_REGEXP = /[\*\>\<\|]+/
      @ctx : ::Medup::Context = ::Medup::Context.new
      @logger : Logger = Logger.new(STDERR)
      @client : ::Medium::Client = ::Medium::Client.new

      JSON.mapping(
        {
          name:              String?,
          type:              Int64,
          text:              String?,
          markups:           Array(ParagraphMarkup)?,
          metadata:          ParagraphMetadata?,
          layout:            Int64?,
          hasDropCap:        Bool?,
          dropCapImage:      DropCapImage?,
          iframe:            Iframe?,
          mixtape_metadata:  {type: MixtapeMetadata, nilable: true, key: "mixtapeMetadata"},
          href:              String?,
          alignment:         Int64?,
          codeBlockMetadata: CodeBlockMetadata?,
        },
        strict: true
      )

      def to_md(
        ctx : ::Medup::Context = ::Medup::Context.new,
        filename_prefix : String = "",
        client = ::Medium::Client.new(ctx: ctx),
      ) : Tuple(String, String, String)
        @ctx = ctx
        settings = ctx.settings
        @logger = ctx.logger
        @client = client
        text : String = @text || ""
        assets = ""
        asset_name = ""
        assets_base_path = settings.assets_base_path
        content : String = case @type
        when 1
          markup
        when 2
          "# #{markup}"
        when 3
          "# #{markup}"
        when 4
          m = metadata
          if !m.nil? && !m.id.nil?
            asset_body, asset_type, asset_name = download_image(m.id || "")
            asset_id = Base64.strict_encode(m.id)
            if settings.assets_image?
              assets = asset_body
              asset_name = "#{filename_prefix}#{asset_name}"
              img = "![#{text}](#{assets_base_path}/#{asset_name})"
            else
              assets = "[image_ref_#{asset_id}]: data:#{asset_type};base64,"
              img = "![#{text}][image_ref_#{asset_id}]"
              assets += Base64.strict_encode(asset_body)
            end
            img = "[#{img}](#{@href})" if @href
            img
          else
            ""
          end
        when 6
          "> #{markup}"
        when 7
          ">> #{markup}"
        when 8
          "```\n#{text}\n```"
        when 9
          "* #{markup}"
        when 10
          "1. #{markup}"
        when 11
          result, asset_name, assets = iframe_markup(filename_prefix)
          result
        when 13
          "### #{markup}"
        when 14
          "#{@mixtape_metadata.try &.href}"
        when 15
          ""
        else
          raise "Unknown paragraph type #{@type} with text #{@text}"
        end
        return content, asset_name, assets
      end

      def download(name : String)
        @client.media(name)
      end

      def download_image(name : String)
        @client.media_image(name)
      end

      def iframe_markup(filename_prefix : String = "")
        asset_name = ""
        assets = ""

        frame = @iframe
        if frame.nil?
          return "<!-- Missing iframe -->", asset_name, assets
        end

        result = ""

        # thumbnailUrl => "https://i.embed.ly/1/image?url=https%3A%2F%2Fi.ytimg.com%2Fvi%2FUULlRc7RBQQ%2Fhq2.jpg&key=4fce0568f2ce49e8b54624ef71a8a5bd"
        if thumbnail_url = frame.thumbnailUrl
          thumbnail_uri = URI.parse(thumbnail_url)
          if thumbnail_uri.query_params.has_key?("url")
            thumbnail_url = thumbnail_uri.query_params["url"]
            if thumbnail_url.starts_with?("https://i.ytimg.com/vi/")
              result = process_youtube_frame(frame, thumbnail_url)
            end
          end
        end

        if result.empty?
          media_content, media_type = download(frame.mediaResourceId)
          @logger.info 12, "media_type: " + media_type if media_type
          @logger.info 12, "media_content: " + media_content

          if media_content.includes?(%{<script src="https://gist.github.com/})
            result = process_gist_content(media_content)
          elsif media_content.includes?("schema=youtube")
            result = process_youtube_content(media_content)
          elsif media_content.includes?("schema=twitter") || media_content.includes?("https://twitter.com")
            result = process_twitter_content(media_content)
          elsif media_content.includes?("cdn.embedly.com")
            result = process_embedly_content(media_content, frame.iframeWidth, frame.iframeHeight)
          end
        end

        if result.empty?
          asset_id = Zaru.sanitize!(frame.mediaResourceId)
          asset_name = "#{filename_prefix}#{asset_id}.html"
          asset_body, _content_type = download(frame.mediaResourceId)
          assets = asset_body
          result = "<iframe src=\"./assets/#{asset_name}\"></iframe>"
        end

        markup_body = markup.strip
        if !markup_body.empty?
          result += "\n\n" if !result.empty?
          result += markup_body
        end

        return result, asset_name, assets
      end

      def process_youtube_frame(frame, thumbnail_url : String) : String?
        @logger.debug 7, "Processing youtube element with thumbnail_url"
        id = thumbnail_url[23..].split("/", 2)[0]
        url = "https://www.youtube.com/watch?v=#{id}"
        "[![Youtube](#{thumbnail_url})](#{url})"
      end

      def process_youtube_content(content : String) : String
        @logger.debug 7, "Processing youtube element"

        m = content.match(/\<iframe[^\>]*widgets\/media\.html\?.*(&amp;)?url=(?<url>[^&;]*)&amp;/)
        return "" if m.nil?
        m = m.not_nil!
        url = URI.decode(m["url"]).sub("http://", "https://")
        # url # => "https://www.youtube.com/watch?v=30xiI21RraQ"
        # url # => "https://www.youtube.com/shorts/UULlRc7RBQQ"
        id = url.sub("https://www.youtube.com/shorts/", "").sub("https://www.youtube.com/watch?v=", "")
        # thumbnail_url = "https://img.youtube.com/vi/#{id}/hqdefault.jpg"
        thumbnail_url = "https://img.youtube.com/vi/#{id}/maxres2.jpg"
        # TODO: Download thumbnails in same way as images with `download_image`
        "[![Youtube](#{thumbnail_url})](#{url})"
      end

      def process_embedly_content(content : String, width : Int64?, height : Int64?) : String
        @logger.debug 7, "Processing embedly element"

        m = content.match(/\<iframe[^\>]*src="(?<src>[^"]*)"/)
        return "" if m.nil? || m["src"].empty?
        embedly_url = URI.parse(m["src"])
        params = embedly_url.query_params

        schema = params["schema"]
        alt_text = params["display_name"]? || schema
        thumbnail_url = params["image"]? || ""
        src = params["src"]

        result = %{__#{schema}__:\n}
        image = schema
        if !thumbnail_url.empty?
          image = %|<img height="#{height}" alt="#{alt_text}" src="#{thumbnail_url}"></img>|
        end

        if src.empty?
          result += image
        else
          result += "\n[#{image}](#{src})"
        end

        result
      end

      def fetch_gist(id : String?)
        @client.fetch_gist(id)
      end

      def process_gist_content(content : String) : String
        @logger.debug 7, "Processing gist element"

        m = content.match(/\<script src=\"https:\/\/gist\.github\.com\/[^\/]*\/(?<id>[^"]*).js/)
        return "" if m.nil?

        gists = fetch_gist(m["id"])

        return "" if gists.nil?

        return gists.map do |gist|
          "```\n#{gist["content"]}\n```\n" \
          "> *[#{gist["filename"]} view raw](#{gist["raw_url"]})*"
        end.join("\n\n")
      end

      def process_twitter_content(content : String) : String
        @logger.debug 7, "Processing twitter element"
        m = content.match(/<meta[^>]*name="description"[^>]*>/m)
        return "" if m.nil?

        m = m[0].match(/content="(?<tweet>[^"]*)"/m)
        return "" if m.nil? || m["tweet"].empty?

        result = "> #{m["tweet"]}\n"

        @logger.debug 12, "Extracting author and tweet's url"
        # <iframe
        # src="https://cdn.embedly.com/widgets/media.html?type=text%2Fhtml&amp;key=a19fcc184b9711e1b4764040d3dc5c07&amp;schema=twitter&amp;url=https%3A//twitter.com/miry_sof/status/1519267429016297473&amp;image=https%3A//i.embed.ly/1/image%3Furl%3Dhttps%253A%252F%252Fabs.twimg.com%252Ferrors%252Flogo46x38.png%26key%3Da19fcc184b9711e1b4764040d3dc5c07"
        # allowfullscreen frameborder="0" scrolling="no"></iframe>
        m = content.match(/\<iframe[^\>]*src="(?<src>[^"]*)"/)

        if !m.nil? && !m["src"].empty?
          embedly_url = URI.parse(m["src"])
          params = embedly_url.query_params

          # https://twitter.com/miry_sof/status/1519267429016297473
          tweet_url = params["url"]
          # NOTE: Cut domain name and extract username from url
          author = tweet_url[20..].split('/')[0]

          return result + "> - [@#{author}](#{tweet_url})"
        end

        m = content.match(/<blockquote[^>]*class="twitter-tweet"[^>]*>(?<quote>.*)<\/blockquote>/m)
        if !m.nil? && !m["quote"].empty?
          return result + "> " + m["quote"] + "\n"
        end

        return result
      end

      def markup
        open_elements = {} of Int64 => Array(ParagraphMarkup)
        close_elements = {} of Int64 => Array(ParagraphMarkup)

        result : String = ""
        _markups = @markups
        return result if _markups.nil?
        _markups.each do |m|
          if open_elements.has_key?(m.start)
            open_elements[m.start] << m
          else
            open_elements[m.start] = [m] of ParagraphMarkup
          end

          if close_elements.has_key?(m.end)
            close_elements[m.end].insert(0, m)
          else
            close_elements[m.end] = [m] of ParagraphMarkup
          end
        end

        text : String = @text || ""
        text += " "
        char_index = 0
        remember_last_space = false
        # Grapheme is an experimental feature from Crystal to return symbols as
        # rendered, instead of static width bytes. It helpes to easy identify
        # emoji symbols chain.
        text.each_grapheme do |symbol|
          if close_elements.has_key?(char_index)
            remember_last_space = false

            if result.ends_with?(" ")
              @logger.warn 4, "Space is before closing element. Swapping order."
              @logger.debug 7, "Swapping just after text: #{(result[-30..]? || result).inspect}"
              remember_last_space = true
              result = result.rchop
            end

            closing = close_elements[char_index]
            closing.sort! { |a, b| b.start <=> a.start }
            closing.each do |m|
              case m.type
              when 1 # bold
                result += "**"
              when 2 # italic
                result += "*"
              when 3 # link
                if result.ends_with?("[")
                  @logger.warn 4, "Skip link because there is no anchor text"
                  result = result.delete_at(-1)
                else
                  href = m.href.nil? ? "" : m.href.not_nil!
                  result += "](#{href})"
                end
              when 10 # code inline
                result += "`"
              else
                raise "Unknown markup type #{m.type} with text #{text[m.start...m.end]}"
              end
            end
            result += " " if remember_last_space
          end

          if open_elements.has_key?(char_index)
            open_elements[char_index].each do |m|
              case m.type
              when 1 # bold
                result += "**"
              when 2 # italic
                result += "*"
              when 3 # link
                result += "["
              when 10 # code inline
                result += "`"
              else
                raise "Unknown markup type #{m.type} with text #{text[m.start...m.end]}"
              end
            end
          end

          result += symbol.to_s
          # Medium count each emoji symbol as 4 bytes, when Crystal uses 2 bytes.
          char_index += symbol.size == 1 ? 1 : symbol.size * 2
        end

        result.rchop
      end

      class ParagraphMarkup
        JSON.mapping(
          type: Int64,
          start: Int64,
          "end": Int64,
          href: String?,
          title: String?,
          rel?: String?,
          anchorType: Int64?
        )
      end

      class Iframe
        getter content : String?

        JSON.mapping(
          mediaResourceId: String,
          thumbnailUrl: String?,
          iframeWidth: Int64?,
          iframeHeight: Int64?
        )

        def get
          @content ||= Medium::Client.default.media(mediaResourceId)
        end
      end

      class MixtapeMetadata
        JSON.mapping(
          mediaResourceId: String,
          href: String?
        )
      end

      class DropCapImage
        JSON.mapping(
          id: String,
          originalWidth: Int64,
          originalHeight: Int64
        )
      end

      class CodeBlockMetadata
        JSON.mapping(
          mode: Int64,
          lang: String,
        )
      end
    end
  end
end

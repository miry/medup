require "uri"

require "./error"

module Medium
  module Connection
    HOST                                      = "medium.com"
    DEFAULT_PARAMS                            = {"format" => "json"}
    ASSETS_FILENAME_SPECIAL_CHARACTERS_REGEXP = /[\*\>\<\|]+/

    def get(endpoint, params : Hash(String, String)? = nil, headers : HTTP::Headers? = nil, body : String? = nil)
      params = DEFAULT_PARAMS.merge(params || Hash(String, String).new)
      request_json "GET", endpoint, params, headers, body
    end

    def http(host : String? = HOST, format : String = "json") : HTTP::Client
      host ||= HOST
      if @http.has_key?(host)
        return @http[host].not_nil!
      end

      _http = HTTP::Client.new host, port: 443, tls: true
      _http.connect_timeout = 3.seconds

      _http.before_request do |request|
        request.headers["Connection"] = "Keep-Alive"
        request.headers["User-Agent"] = "medup/#{Medup::VERSION}"
        if format == "json"
          request.headers["Content-Type"] = "application/json"
          request.headers["Accept"] = "*/*"
        end
        @logger.info 12, "Request headers: #{request.headers.inspect}"
      end

      @http[host] = _http
      return _http
    end

    def request(method, endpoint, params : Hash(String, String)? = nil, headers : HTTP::Headers? = nil, body : String? = nil, format : String = "json")
      uri = URI.parse endpoint
      uri.host ||= HOST
      uri.scheme ||= "https"

      if params
        o = uri.query_params
        params.each { |k, v| o.add(k, v) }
        uri.query_params = o
      end

      response = http_exec(uri, method, headers, body, format)

      limit = 10
      while limit > 0 && response.status_code >= 300 && response.status_code < 400
        redirect_uri = URI.parse(response.headers["location"])
        redirect_uri.host ||= uri.host
        redirect_uri.scheme ||= uri.scheme
        uri = redirect_uri
        response = http_exec(uri, method, headers, body, format)
        limit -= 1
      end

      mime = response.mime_type
      if mime && %w[text application].includes?(mime.type)
        @logger.info 12, response.body
      end

      error = Medium::Error.from_response(response)
      raise error if error

      response
    end

    def http_exec(uri, method, headers, body, format = "json")
      response = http(uri.host, format).exec(method: method.upcase, path: uri.request_target, headers: headers, body: body)
      @logger.info "#{method} #{uri} => #{response.status_code} #{response.status_message}"
      @logger.info 12, response.headers.to_s
      response
    end

    def request_json(method, endpoint, params : Hash(String, String)? = nil, headers : HTTP::Headers? = nil, body : String? = nil)
      response = request(method, endpoint, params, headers, body, "json")

      error = Medium::Error.from_json_response(response)
      raise error if error

      JSON.parse(response.body[16..])
    end

    def download(endpoint : String)
      response = request("GET", endpoint, format: "json")
      return response.body, response.content_type
    end

    def download_image(endpoint : String, name : String)
      response = request("GET", endpoint, format: "json")

      filename = name
      ext = File.extname(filename)
      if ext == ""
        mt = response.mime_type
        if !mt.nil? && !mt.sub_type.nil?
          filename += "." if filename[-1] != '.'
          filename += mt.sub_type.not_nil!
        end
      end
      filename = filename.gsub(ASSETS_FILENAME_SPECIAL_CHARACTERS_REGEXP, '_')

      return response.body, response.content_type, filename
    end
  end
end

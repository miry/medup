# https://medium.com/media/e7722acf2886364130e03d2c7ad29de7
module Medium
  class Client
    module Media
      def media(id : String)
        download("https://medium.com/media/#{id}")
      end

      def media_image(id : String)
        download_image("https://miro.medium.com/v2/#{id}", id)
      end

      def fetch_gist(id : String?, skip_token = false)
        return nil if id.nil?

        src = "https://api.github.com/gists/#{id}"
        uri = URI.parse src
        response : HTTP::Client::Response? = nil

        headers = HTTP::Headers{
          "Content-Type"         => "application/json",
          "X-GitHub-Api-Version" => "2022-11-28",
        }

        if !skip_token
          token = @ctx.settings.github_api_token
          if token && !token.empty?
            headers.add("Authorization", "token #{token}")
          end
        end

        3.times do
          response = http_exec(uri, "GET", headers, nil, format: "json")
          break if response.status_code != 403
          sleep 3.seconds # 3 seconds
        end

        return nil if response.nil?

        if response.status_code == 403
          puts "Warning: Reached github api rate limit."
          puts "Create a GitHub personal access token:"
          puts "  https://github.com/settings/tokens/new?description=#{Time.local.to_s("%Y-%m-%d")}_medup&scopes="
          puts "echo 'export MEDUP_GITHUB_API_TOKEN=your_token_here' >> .profile"

          return nil
        end

        if response.status_code == 401
          message = JSON.parse(response.body)
          puts "Warning: Error fetch gist from GitHub: GitHub API Error: #{message}"
          puts "MEDUP_GITHUB_API_TOKEN may be invalid or expired; check:"
          puts "  https://github.com/settings/tokens"

          # Try again without authorization token
          return skip_token ? nil : fetch_gist(id, skip_token: true)
        end

        result = JSON.parse(response.body).try(&.["files"]?).try(&.as_h)
        return nil if result.nil?

        return result.map { |_, spec| spec.as_h }
      end
    end
  end
end

# docker.io/miry/medup
# codeberg.org/miry/medup

# Build layer
ARG CRYSTAL_VERSION=1.15.1
ARG USER=1001

# NOTE: docker.io/crystallang/crystal has only single platform.
#       To avoid access to internet on each build, requires to specify the platform.
FROM --platform=linux/amd64 docker.io/crystallang/crystal:${CRYSTAL_VERSION}-alpine as build

# Install development tools required for building
RUN apk --no-cache add \
    ruby-rake \
    ruby-json \
 && mkdir /app \
 && chown 1001:1001 /app

USER ${USER}

# Initialoze the working directory
WORKDIR /app

# NOTE: Implementation caching to install shards.
#       Uncomment when libs is removed from the git repo.
# # Cache install package dependicies
# COPY ./shard.* /app/
# RUN shards install --production -v
# COPY ./lib/*.cr /app/lib/

# Build the app
COPY lib /app/lib
COPY src /app/src
COPY shard.* Rakefile /app/
RUN --mount=type=cache,target=/root/.cache rake build:static \
 && chmod 555 /app/_output/medup

# Runtime layer
FROM scratch as runtime

USER ${USER}

WORKDIR /

# Copy/install required assets like CA certificates
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/cert.pem
COPY --from=build /app/_output/medup .

ENTRYPOINT ["/medup"]

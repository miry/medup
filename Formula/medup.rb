class Medup < Formula
  desc "Download content from Medium and Dev.to as Markdown files into a local folder."
  homepage "http://codeberg.org/miry/medup"
  url "https://codeberg.org/miry/medup/archive/v0.11.3.tar.gz"
  sha256 "cf22f17f19edefc8154d616ec23558d075b77ac8dddcaff89f6157753f5ee73e"
  license "LGPL-3.0-only"
  head "https://codeberg.org/miry/medup.git"

  livecheck do
    url :head
    regex(/^v?(\d+(?:\.\d+)+)$/i)
  end

  depends_on "crystal"

  def install
    system "crystal", "build", "--release", "--debug", "-o", "medup", "src/cli.cr"
    bin.install "medup"
  end

  test do
    system "{bin}/medup", "--version"
  end
end

require "spec"
require "webmock"

require "../src/medup"

Spec.before_suite do
  WebMock.reset

  io = IO::Memory.new
  Base64.decode("iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mP8/x8AAwMCAO+ip1sAAAAASUVORK5CYII=", io)
  WebMock.stub(:get, "https://miro.medium.com/0*FbFs8aNmqNLKw4BM")
    .to_return(
      body: "",
      status: 301,
      headers: HTTP::Headers{
        "Location"       => "/v2/0*FbFs8aNmqNLKw4BM",
        "Content-Type"   => "application/octet-stream",
        "Content-length" => "0",
      }
    )

  WebMock.stub(:get, "https://miro.medium.com/v2/0*FbFs8aNmqNLKw4BM")
    .to_return(
      body: io.to_s,
      headers: HTTP::Headers{
        "Content-Type"   => "image/jpeg",
        "Content-length" => "2880057",
      }
    )

  WebMock.stub(:get, "https://miro.medium.com/v2/1*NVLl4oVmMQtumKL-DVV1rA.png")
    .to_return(
      body: "",
      status: 301,
      headers: HTTP::Headers{
        "Location"       => "/v2/1*NVLl4oVmMQtumKL-DVV1rA.png",
        "Content-Type"   => "image/png",
        "Content-length" => "0",
      }
    )

  WebMock.stub(:get, "https://miro.medium.com/v2/1*NVLl4oVmMQtumKL-DVV1rA.png")
    .to_return(body: "some binary content", headers: HTTP::Headers{"Content-Type" => "image/png"})

  WebMock.stub(:get, "https://medium.com/media/e7722acf2*886364130e03d2c7ad29de7")
    .to_return(body: "<div>Iframe example</div>", headers: HTTP::Headers{"Content-Type" => "text/html"})

  WebMock.stub(:get, "https://medium.com/media/ab24f0b378f797307fddc32f10a99685")
    .to_return(body: "<div>Iframe example</div>", headers: HTTP::Headers{"Content-Type" => "text/html"})

  WebMock.stub(:get, "https://medium.com/media/983a8eccc279e93060af2a7f7a3a0760")
    .to_return(body: medium_twitter_iframe, headers: HTTP::Headers{"Content-Type" => "text/html"})

  WebMock.stub(:get, "https://medium.com/media/5193f6915f7463ffd5ebf786f2d7bc51")
    .to_return(body: medium_iframe_soundcloud, headers: HTTP::Headers{"Content-Type" => "text/html"})

  WebMock.stub(:get, "https://medium.com/media/6c0e5f85caff38fa1e790100a2159cd7")
    .to_return(body: medium_iframe_codepen, headers: HTTP::Headers{"Content-Type" => "text/html"})
end

def fixtures_raw(name)
  File.read(File.join("spec", "fixtures", name))
end

def fixtures(name)
  JSON.parse(fixtures_raw(name))
end

def post_fixture
  data = fixtures("post_response.json")
  data["payload"]["value"].to_json
end

def post_without_name_fixture
  data = fixtures("post_without_name_response.json")
  data["payload"]["value"].to_json
end

def post_without_dropimage_fixture
  data = fixtures("post_without_dropimage.json")
  data["payload"]["value"].to_json
end

def user_fixture
  data = fixtures("post_response.json")
  creator_id = data["payload"]["value"]["creatorId"].as_s
  data["payload"]["references"]["User"][creator_id].to_json
end

def medium_twitter_iframe
  fixtures_raw "medium_iframe_twitter.html"
end

def medium_iframe_gist
  fixtures_raw "medium_iframe_gist.html"
end

def medium_iframe_soundcloud
  fixtures_raw "medium_iframe_soundcloud.html"
end

def medium_iframe_codepen
  fixtures_raw "medium_iframe_codepen.html"
end

def logger
  Logger.new(STDOUT, level: 6)
end

def context
  ::Medup::Context.new(logger: logger)
end

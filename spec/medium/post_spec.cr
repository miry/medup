require "../spec_helper"

describe Medium::Post do
  describe "initialize" do
    it "initialize from json" do
      subject = Medium::Post.from_json(post_fixture)
      subject.should_not eq(nil)
    end

    it "setups title" do
      subject = Medium::Post.from_json(post_fixture)
      subject.title.should eq("Modify binary files with VIM")
    end

    it "setups slug" do
      subject = Medium::Post.from_json(post_fixture)
      subject.slug.should eq("modify-binary-files-with-vim")
    end

    it "setups content" do
      subject = Medium::Post.from_json(post_fixture)
      subject.content.bodyModel.paragraphs.size.should eq(26)
    end

    it "missing name" do
      subject = Medium::Post.from_json(post_without_name_fixture)
      subject.content.bodyModel.paragraphs.size.should eq(58)
    end

    it "missing drop cap image" do
      subject = Medium::Post.from_json(post_without_dropimage_fixture)
      subject.content.bodyModel.paragraphs.size.should eq(39)
    end
  end

  describe "#to_md" do
    it "canonical url" do
      WebMock.stub(:get, "https://miro.medium.com/v2/0*wSoA3zqzobeU3Nwx.jpeg")
        .to_return(body: "")
      subject = Medium::Post.from_json(post_without_name_fixture)
      content, assets = subject.to_md
      content.should contain <<-HEADER
      canonical_url: https://medium.com/@seenmyfate/capistrano-version-3-ba896a142ac?webCanonicalUrl
      HEADER
    end

    it "render full page" do
      subject = Medium::Post.from_json(post_fixture)
      content, assets = subject.to_md
      content.size.should eq(4548)
      assets.size.should eq(1)
      assets.keys.should eq([
        "2019-01-13-modify-binary-files-with-vim-ab24f0b378f797307fddc32f10a99685.html",
      ])
    end

    it "render full page without images" do
      subject = Medium::Post.from_json(post_fixture)
      settings = ::Medup::Settings.new
      settings.set_assets_image!
      subject.ctx = ::Medup::Context.new(settings)

      content, assets = subject.to_md
      content.size.should eq(4332)
      assets.keys.should eq([
        "2019-01-13-modify-binary-files-with-vim-0_FbFs8aNmqNLKw4BM.jpeg",
        "2019-01-13-modify-binary-files-with-vim-ab24f0b378f797307fddc32f10a99685.html",
      ])
    end

    it "renders header" do
      subject = Medium::Post.from_json(post_fixture)
      paragraph = subject.content.bodyModel.paragraphs[0]
      paragraph.to_md[0].should eq("# Modify binary files with VIM")
    end

    it "renders blockquotes" do
      subject = Medium::Post.from_json(post_fixture)
      paragraph = subject.content.bodyModel.paragraphs[1]
      paragraph.to_md[0].should eq("> TL;DR `xxd ./bin/app | vim —` and `:%!xxd -r > ./bin/new_app`")
    end

    it "renders image" do
      subject = Medium::Post.from_json(post_fixture)
      paragraph = subject.content.bodyModel.paragraphs[2]
      actual = paragraph.to_md
      actual[0].should eq("![Photo by Markus Spiske on Unsplash][image_ref_MCpGYkZzOGFObXFOTEt3NEJN]")
      actual[1].should eq("0_FbFs8aNmqNLKw4BM.jpeg")
      actual[2][0..64].should eq("[image_ref_MCpGYkZzOGFObXFOTEt3NEJN]: data:image/jpeg;base64,iVBO")
    end

    it "renders image in assets" do
      subject = Medium::Post.from_json(post_fixture)
      settings = ::Medup::Settings.new
      settings.set_assets_image!
      ctx = ::Medup::Context.new(settings)

      paragraph = subject.content.bodyModel.paragraphs[2]
      actual = paragraph.to_md(ctx)
      actual[0].should eq("![Photo by Markus Spiske on Unsplash](./assets/0_FbFs8aNmqNLKw4BM.jpeg)")
      actual[1].should eq("0_FbFs8aNmqNLKw4BM.jpeg")
      actual[2].size.should eq(66)
    end

    describe "paragraph text" do
      it "renders content with capital letter" do
        subject = Medium::Post.from_json(post_fixture)
        paragraph = subject.content.bodyModel.paragraphs[3]
        paragraph.to_md[0].should contain("When I was a student")
      end

      it "renders content with links" do
        subject = Medium::Post.from_json(post_fixture)
        paragraph = subject.content.bodyModel.paragraphs[3]
        paragraph.to_md[0].should contain("like [Total Commander](https://www.ghisler.com/) or [FAR manager](https://www.farmanager.com/)")
      end

      it "renders content with bold text with link" do
        subject = Medium::Post.from_json(post_fixture)
        paragraph = subject.content.bodyModel.paragraphs[7]
        paragraph.to_md[0].should contain("I came to: [**xxd**](http://vim.wikia.com/wiki/Hex_dump)")
      end

      it "renders content with inline code block" do
        subject = Medium::Post.from_json(post_fixture)
        paragraph = subject.content.bodyModel.paragraphs[7]
        paragraph.to_md[0].should contain(%{of `vim-common` package})
      end
    end

    it "renders numered list first" do
      subject = Medium::Post.from_json(post_fixture)
      paragraph = subject.content.bodyModel.paragraphs[5]
      paragraph.to_md[0].should eq("1. it should be cross distributive solution")
    end

    it "renders numered list second" do
      subject = Medium::Post.from_json(post_fixture)
      paragraph = subject.content.bodyModel.paragraphs[6]
      paragraph.to_md[0].should eq("1. should be easy to use with Vim (as main my editor for linux machines)")
    end

    pending "render split" do
      subject = Medium::Post.from_json(post_fixture)
      paragraph = subject.content.bodyModel.paragraphs[12]
      paragraph.to_md[0].should eq("---")
    end

    it "render iframe" do
      subject = Medium::Post.from_json(post_fixture)
      paragraph = subject.content.bodyModel.paragraphs[12]
      paragraph.to_md[0].should eq("<iframe src=\"./assets/ab24f0b378f797307fddc32f10a99685.html\"></iframe>")
    end

    it "render twitter frame" do
      subject = Medium::Post.from_json(post_fixture)
      paragraph = subject.content.bodyModel.paragraphs[13]
      paragraph.to_md[0].should eq <<-TEXT
      > With https://t.co/MSHpm3lVIj I see how often my favorites articles were changed and see previous history of content. I am thinking to create a service to show archives of #medium articles or export to markdown. If you have ideas submit to https://t.co/yEV9W9b2ph
      > - [@miry_sof](https://twitter.com/miry_sof/status/1519267429016297473)

      Caption for the tweet message
      TEXT
    end

    it "render code block" do
      subject = Medium::Post.from_json(post_fixture)
      paragraph = subject.content.bodyModel.paragraphs[16]
      paragraph.to_md[0].should contain(%{```\n[terminal 1]})
    end

    it "understands alignment attribute" do
      # Original: https://medium.com/@jico/the-fine-art-of-javascript-error-tracking-bc031f24c659
      subject = Medium::Post.from_json(post_fixture)
      paragraph = subject.content.bodyModel.paragraphs[22]
      paragraph.to_md[0].should contain(%{# The Title with image})
    end

    it "render soundcloud frame" do
      subject = Medium::Post.from_json(post_fixture)
      paragraph = subject.content.bodyModel.paragraphs[23]
      paragraph.to_md[0].should eq <<-TEXT
      __soundcloud__:

      [<img height="166" alt="SoundCloud" src="https://i1.sndcdn.com/artworks-WxvfCeEA7YqcN30t-DlA82Q-t500x500.jpg"></img>](https://w.soundcloud.com/player/?url=https%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F834983812&show_artwork=true)

      Soundcloud: Something for ya ears while you read.
      TEXT
    end

    it "render codepen frame" do
      subject = Medium::Post.from_json(post_fixture)
      paragraph = subject.content.bodyModel.paragraphs[24]
      paragraph.to_md[0].should eq <<-TEXT
      __codepen__:

      [<img height="600" alt="CodePen" src="https://shots.codepen.io/username/pen/WqrrWK-512.jpg?version=1653663802"></img>](https://codepen.io/andriyparashchuk/embed/preview/WqrrWK?default-tabs=css%2Cresult&height=600&host=https%3A%2F%2Fcodepen.io&slug-hash=WqrrWK)

      [https://codepen.io/andriyparashchuk/pen/WqrrWK?editors=1100](https://codepen.io/andriyparashchuk/pen/WqrrWK?editors=1100)
      TEXT
    end

    it "parse paragraph without text field" do
      subject = Medium::Post.from_json(post_fixture)
      paragraph = subject.content.bodyModel.paragraphs[25]
      paragraph.to_md[0].should eq("![][image_ref_MSpOVkxsNG9WbU1RdHVtS0wtRFZWMXJBLnBuZw==]")
    end

    describe "metadata" do
      it "stores description information in metadata" do
        subject = Medium::Post.from_json(post_fixture)
        subject.to_md[0].should contain(%{description: Sometime I need to open binaries})
      end

      it "stores subtitle information in metadata" do
        subject = Medium::Post.from_json(post_fixture)
        subject.to_md[0].should contain(%{subtitle: Edit binary files in Linux with Vim})
      end

      it "stores tags information in metadata" do
        subject = Medium::Post.from_json(post_fixture)
        subject.to_md[0].should contain <<-HEADER
        tags:
        - vim
        - debug
        - linux
        - cracking
        - hacking
        HEADER
      end

      it "stores author name information in metadata" do
        subject = Medium::Post.from_json(post_fixture)
        user = Medium::User.from_json(user_fixture)
        subject.user = user
        subject.to_md[0].should contain(%{author: Michael Nikitochkin})
      end

      it "stores username information in metadata" do
        subject = Medium::Post.from_json(post_fixture)
        user = Medium::User.from_json(user_fixture)
        subject.user = user
        subject.to_md[0].should contain(%{username: miry})
      end
    end
  end

  describe "#filename" do
    it "uses date with slug" do
      WebMock.stub(:get, "https://miro.medium.com/v2/0*wSoA3zqzobeU3Nwx.jpeg")
        .to_return(body: "")
      subject = Medium::Post.from_json(post_without_name_fixture)
      subject.filename.should eq("2013-08-07-capistrano-version-3")
    end
  end
end

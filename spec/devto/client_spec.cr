require "../spec_helper"

describe Devto::Client do
  describe "#convert_to_api_url" do
    it "converts basic url" do
      subject = ::Devto::Client.new(::Medup::Context.new)
      actual = subject.convert_to_api_url("https://dev.to/jetthoughts/how-to-use-linear-gradient-in-css-bi1")
      expected = "https://dev.to/api/articles/jetthoughts/how-to-use-linear-gradient-in-css-bi1"
      actual.should eq(expected)
    end
  end

  describe "#post_urls_by_author" do
    it "uses path from the response" do
      articles_response = File.read(File.join("spec", "fixtures", "devto_articles.json"))

      # Mock first request to get aritcles for page 1
      WebMock.stub(:get, "https://dev.to/api/articles?username=foo&page=1&per_page=1000")
        .to_return(
          body: articles_response,
          headers: HTTP::Headers{"Content-Type" => "application/json"}
        )

      # Mock second request to get empty list for page 2
      WebMock.stub(:get, "https://dev.to/api/articles?username=foo&page=2&per_page=1000")
        .to_return(
          body: "[]",
          headers: HTTP::Headers{"Content-Type" => "application/json"}
        )

      subject = ::Devto::Client.new(::Medup::Context.new)
      actual = subject.post_urls_by_author("foo")
      actual.should contain "https://dev.to/api/articles/jetthoughts/minimalistic-web-server-for-static-files-with-crystal-19a1"
    end
  end

  describe "#get" do
    it "handles 429 gracefully" do
      url = "https://dev.to/api/articles/foo/bar-39lg"
      WebMock.stub(:get, url)
        .to_return(
          status: 429,
          body: "something goes wrong",
          headers: HTTP::Headers{"Content-Type" => "text/plain"}
        )
      subject = ::Devto::Client.new(::Medup::Context.new)

      expect_raises(::Devto::ClientError, "Too Many Requests") do
        response = subject.get(url)
      end
    end
  end
end

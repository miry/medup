require "../spec_helper"

describe Devto::Error do
  describe ".from_response" do
    it "covers error message for 404" do
      response_body = %|{"error":"not found","status":404}|
      headers = HTTP::Headers{
        "content-type"   => "application/json; charset=utf-8",
        "content-length" => "#{response_body.size}",
      }

      response = HTTP::Client::Response.new(status_code: 404, body: response_body, headers: headers)
      error = ::Devto::Error.from_response(response)
      error.should be_a(Devto::ClientError)
    end

    it "covers error message for 429" do
      response_body = %|too many requests|
      headers = HTTP::Headers{
        "content-type"   => "text/plain; charset=utf-8",
        "content-length" => "#{response_body.size}",
      }

      response = HTTP::Client::Response.new(status_code: 429, body: response_body, headers: headers)
      error = ::Devto::Error.from_response(response)
      error.should be_a(Devto::ClientError)
    end
  end
end

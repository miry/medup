require "../spec_helper"

describe Devto::Post do
  describe ".from_json" do
    it "loads sample post" do
      subject = Devto::Post.from_json(%|{"id": 1}|)
      subject.slug.should eq("")
      subject.id.should eq(1)
    end

    it "sets slug" do
      subject = Devto::Post.from_json(%|{"id": 1, "slug": "how-to-use-linear-gradient-in-css-bi1"}|)
      subject.slug.should eq("how-to-use-linear-gradient-in-css-bi1")
    end

    it "tests the fixture" do
      content = fixtures_raw "devto_article_trial_period.json"
      subject = Devto::Post.from_json content
      subject.slug.should eq("the-trial-period-for-staff-augmentation-in-jetthoughts-1h5c")
    end
  end

  describe "#filename" do
    it "generates date and slug" do
      content = fixtures_raw "devto_article_trial_period.json"
      subject = Devto::Post.from_json content
      subject.filename.should eq("2020-09-22-the-trial-period-for-staff-augmentation-in-jetthoughts-1h5c")
    end
  end

  describe "#to_md" do
    before_each do
      WebMock.stub(:get, "https://media2.dev.to/dynamic/image/width=1000,height=420,fit=cover,gravity=auto,format=auto/https%3A%2F%2Fdev-to-uploads.s3.amazonaws.com%2Fuploads%2Farticles%2Flu9okrqilodwp0pqp7cf.jpeg")
        .to_return(
          body: %{image content},
          headers: HTTP::Headers{"Content-Type" => "image/jpeg"})
    end

    it "renders a sample page" do
      content = fixtures_raw "devto_article_trial_period.json"
      subject = Devto::Post.from_json content
      content, assets = subject.to_md
      content.size.should eq 2685
      assets.size.should eq 1
    end

    it "renders the header section" do
      content = fixtures_raw "devto_article_trial_period.json"
      subject = Devto::Post.from_json content
      content, assets = subject.to_md
      content.should contain <<-HEADER
      ---
      url: https://dev.to/jetthoughts/the-trial-period-for-staff-augmentation-in-jetthoughts-1h5c
      canonical_url: https://jtway.co/the-trial-period-in-jetthoughts-968e7f01481f?source=friends_link&sk=56dbdb8567ab7500796037d42c80e46a
      title: The Trial Period for Staff Augmentation in JetThoughts
      slug: the-trial-period-for-staff-augmentation-in-jetthoughts-1h5c
      description: We offer a 2-week trial with no obligation. So you can test everything
        and see how it goes with no fi...
      tags:
      - startup
      - engagement
      - team
      - development
      author: JT Dev
      username: jetthoughts_61
      ---
      HEADER
    end

    it "includes username and author" do
      content = fixtures_raw "devto_duplicate_header.json"
      subject = Devto::Post.from_json content
      content, _ = subject.to_md
      content.should contain "username: miry"
      content.should contain "author: IMOC"
    end

    it "skip devto original header section" do
      content = fixtures_raw "devto_duplicate_header.json"
      subject = Devto::Post.from_json content
      content, _ = subject.to_md
      content.should_not contain "published: true"
    end

    it "keep internal dashes" do
      content = fixtures_raw "devto_duplicate_header.json"
      subject = Devto::Post.from_json content
      content, _ = subject.to_md
      content.should contain <<-CONTENT
      # Article title


      Content starts here. Example of inner text usage of delimter or header.
      Header
      ---
      CONTENT
    end

    it "renders title" do
      content = fixtures_raw "devto_article_trial_period.json"
      subject = Devto::Post.from_json content
      content, assets = subject.to_md
      content.should contain <<-HEADER
      # The Trial Period for Staff Augmentation in JetThoughts
      HEADER
    end

    it "renders the cover image" do
      content = fixtures_raw "devto_article_trial_period.json"
      subject = Devto::Post.from_json content
      content, assets = subject.to_md
      content.should contain <<-IMAGE
      ![Cover image][img_ref_cover_image]
      IMAGE

      content.should contain <<-IMAGE
      [img_ref_cover_image]: data:image/jpeg;base64,aW1hZ2UgY29udGVudA==
      IMAGE

      assets.has_key?("img_ref_cover_image").should be_true
    end

    it "renders the cover image in assets folder" do
      content = File.read(File.join("spec", "fixtures", "devto_article_trial_period.json"))
      subject = Devto::Post.from_json content
      settings = ::Medup::Settings.new
      settings.set_assets_image!
      subject.ctx = ::Medup::Context.new(settings)

      content, assets = subject.to_md
      content.should contain <<-IMAGE
      ![Cover image](./assets/2020-09-22-the-trial-period-for-staff-augmentation-in-jetthoughts-1h5c-cover_image-lu9okrqilodwp0pqp7cf.jpeg)
      IMAGE

      assets.has_key?("2020-09-22-the-trial-period-for-staff-augmentation-in-jetthoughts-1h5c-cover_image-lu9okrqilodwp0pqp7cf.jpeg").should be_true
    end
  end

  describe "#md_content" do
    it "parses image tags" do
      WebMock.stub(:get, "https://example.com/image_url1.png")
        .to_return(
          body: %{image content},
          headers: HTTP::Headers{"Content-Type" => "image/jpeg"})
      WebMock.stub(:get, "https://example.com/image_url2.png")
        .to_return(
          body: %{image content},
          headers: HTTP::Headers{"Content-Type" => "image/jpeg"})
      WebMock.stub(:get, "https://example.com/image_url3.png")
        .to_return(
          body: %{image content},
          headers: HTTP::Headers{"Content-Type" => "image/jpeg"})

      subject = Devto::Post.from_json <<-'CONTENT'
      {
        "body_markdown": "# Title\n![Image](https://example.com/image_url1.png) ![Image](https://example.com/image_url2.png) ![Image](https://example.com/image_url3.png)"
      }
      CONTENT
      assets = Hash(String, String).new
      actual = subject.md_content(assets)
      actual.should eq "# Title\n![Image][img_ref_image_url1_png] ![Image][img_ref_image_url2_png] ![Image][img_ref_image_url3_png]"
      assets.size.should eq 3
      assets.keys.sort!.join(" ").should eq "img_ref_image_url1_png img_ref_image_url2_png img_ref_image_url3_png"
    end

    it "parses image tags with assets images" do
      WebMock.stub(:get, "https://example.com/image_url1.png")
        .to_return(
          body: %{image content},
          headers: HTTP::Headers{"Content-Type" => "image/jpeg"})
      WebMock.stub(:get, "https://example.com/image_url2.png")
        .to_return(
          body: %{image content},
          headers: HTTP::Headers{"Content-Type" => "image/jpeg"})
      WebMock.stub(:get, "https://example.com/image_url3.png")
        .to_return(
          body: %{image content},
          headers: HTTP::Headers{"Content-Type" => "image/jpeg"})

      subject = Devto::Post.from_json <<-'CONTENT'
      {
        "created_at": "2023-11-26T16:31:59Z",
        "body_markdown": "# Title\n![Image](https://example.com/image_url1.png) ![Image](https://example.com/image_url2.png) ![Image](https://example.com/image_url3.png)"
      }
      CONTENT
      settings = ::Medup::Settings.new
      settings.set_assets_image!
      subject.ctx = ::Medup::Context.new(settings)

      assets = Hash(String, String).new
      actual = subject.md_content(assets)
      actual.should eq "# Title\n![Image](./assets/2023-11-26-image_url1.png) ![Image](./assets/2023-11-26-image_url2.png) ![Image](./assets/2023-11-26-image_url3.png)"
      assets.size.should eq 3
      assets.keys.sort!.join(" ").should eq "2023-11-26-image_url1.png 2023-11-26-image_url2.png 2023-11-26-image_url3.png"
    end
  end

  describe "#md_cover_image" do
    it "cover image assets id with inline assets" do
      WebMock.stub(:get, "https://example.com/foo.png")
        .to_return(
          body: %{image content},
          headers: HTTP::Headers{"Content-Type" => "image/jpeg"})

      subject = Devto::Post.from_json <<-'CONTENT'
      {
        "created_at": "2023-02-19T16:31:59Z",
        "cover_image": "https://example.com/foo.png",
        "body_markdown": "# Title\nContent"
      }
      CONTENT
      assets = Hash(String, String).new
      actual = subject.md_cover_image(assets)
      actual.should contain "![Cover image][img_ref_cover_image]"
      assets.size.should eq 1
      assets.has_key?("img_ref_cover_image").should be_true
    end

    it "cover image assets id with external assets and empty slug" do
      WebMock.stub(:get, "https://example.com/foo.png")
        .to_return(
          body: %{image content},
          headers: HTTP::Headers{"Content-Type" => "image/jpeg"})

      subject = Devto::Post.from_json <<-'CONTENT'
      {
        "created_at": "2023-02-19T16:31:59Z",
        "cover_image": "https://example.com/foo.png",
        "body_markdown": "# Title\nContent"
      }
      CONTENT
      settings = ::Medup::Settings.new
      settings.set_assets_image!
      subject.ctx = ::Medup::Context.new(settings)

      assets = Hash(String, String).new
      actual = subject.md_cover_image(assets)
      actual.should contain "![Cover image](./assets/2023-02-19-cover_image-foo.png)"
      assets.size.should eq 1
      assets.has_key?("2023-02-19-cover_image-foo.png").should be_true
    end

    it "cover image assets id with external assets" do
      WebMock.stub(:get, "https://example.com/foo.png")
        .to_return(
          body: %{image content},
          headers: HTTP::Headers{"Content-Type" => "image/jpeg"})

      subject = Devto::Post.from_json <<-'CONTENT'
      {
        "slug": "foo-bar-quux",
        "created_at": "2023-02-19T16:31:59Z",
        "cover_image": "https://example.com/foo.png",
        "body_markdown": "# Title\nContent"
      }
      CONTENT
      settings = ::Medup::Settings.new
      settings.set_assets_image!
      subject.ctx = ::Medup::Context.new(settings)

      assets = Hash(String, String).new
      actual = subject.md_cover_image(assets)
      actual.should contain "![Cover image](./assets/2023-02-19-foo-bar-quux-cover_image-foo.png)"
      assets.size.should eq 1
      assets.has_key?("2023-02-19-foo-bar-quux-cover_image-foo.png").should be_true
    end
  end
end

#!/bin/sh

# Usage: release_notes v0.9.0

output_file="tmp/release_changelog.md"
rm -f $output_file

version=${1#v*}

echo "Processing ${version}"
mkdir -p tmp
touch $output_file

echo '## Summary' >> $output_file
echo >> $output_file

echo "Extracting release changes from CHANGELOG.md"
sed '/^## \['$version'\]/,/^## \[/!d;//d;/^\s*$/d' CHANGELOG.md >> $output_file
cat $output_file

echo "Extracting release compare url from CHANGELOG.md"
compare_url=$(cat CHANGELOG.md | grep -e "^\[$version\]" | sed 's/^.*: //')
echo >> $output_file
echo "**Full Changelog**: $compare_url" >> $output_file
echo >> $output_file
echo "url: ${compare_url}"

cat << 'EOF' >> $output_file
## Install
### MacOS

```shell
$ brew tap miry/medup
$ brew install medup
$ medup --help
$ medup -u <user>
$ medup -p <publisher>
$ medup --platform=devto -u <user>
```

### Docker

```shell
EOF

echo '$ docker run --rm -v <path to local articles folder>:/posts -it codeberg.org/miry/medup:'${version}' <@user|url|publication> -v7' >> $output_file
echo '$ docker run --rm -v <path to local articles folder>:/posts -it docker.io/miry/medup:'${version}' <@user|url|publication> -v7' >> $output_file
echo '```' >> $output_file

cat $output_file
